# Maastotietokanta2dxf
mtk2dxf.py skripti MML:n maastotietokannasta irroitettujen shp-tiedostojen muuntamiseen dxf-muotoon Ocadiin lukemista varten. 

Versio 0.1a

Tehnyt Pellervo Kassi
palaute: etunimi piste sukunimi gmail piste com 


Tämä koodi on gpl:n tapaan lisensoitu: saat jakaa ohjelmaa eteenpain, mutta la$


Ocadiin lukemisessa tarvitaan myös mukana toimitettavaa shp2isom.crt tiedostoa.



Vaatimukset
python 2.7 (3-versio ei toimi).


Pythoniin ladattuna:

dxfwrite (http://pypi.python.org/pypi/dxfwrite/1.2.0)

pyshp (http://code.google.com/p/pyshp/)

Molemmat pystyy asentamaan easy_install:in (http://en.wikipedia.org/wiki/EasyInstall) avulla.



Käyttö:

1. Tallenna mtk2dxf.py työhakemistoon.

2. python mtk2dxf.py tiedostonimi

Lukee kaikki .shp-tiedostot työhakemistosta ja tallentaa niiden tiedot tiedostonimi.dxf tiedostoon.

Luotu dxf-tiedosto sisältää shp-tiedoston piste, polygon ja viivamuotoiset muuttujat tallennettuna maastotietokannan luokittelun mukaisille tasoilleen.

Tasojen nimien merkitys maastotietokannassa selviää maastotietokannan kohdemallista: 
http://www.maanmittauslaitos.fi/sites/default/files/Maastotietokanta_kohdemalli.xls


Luotu DXF-tiedosto luetaan sisään Ocadiin käyttämällä Tuo tiedosto toimintoa. 
Kun tiedosto on valittu lisätään crt-tiedosto painamalla näppäintä "crt". 
Ocad kohdetiedoston symbolien tulee olla nimetty ISOM-luokituksen mukaan jotta tuonti tapahtuu oikein. 
Esim. SSL:n symbolitiedosto sopii tähän käyttötarkoitukseen.



En ota mitään vastuuta ohjelman aiheuttamista vahingoista datalle tai omaisuudelle.
Pidä kivaa!

