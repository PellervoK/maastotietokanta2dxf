#mtk2dxf.py skripti MML:n maastotietokannasta irroitettujen shp-tiedostojen
#muuntamiseen dxf-muotoon Ocadiin lukemista varten. 
#Versio 0.1a
#Tehnyt Pellervo Kassi
#palaute: etunimi piste sukunimi gmail piste com
#Tama koodi on gpl:n tapaan lisensoitu: saat jakaa ohjelmaa eteenpain, mutta lahdekoodi on toimitettava pyydettaessa.
#Ocadiin lukemisessa tarvitaan myos mukana toimtettavaa shp2isom.crt tiedostoa.
import os
import sys
import shapefile

if len(sys.argv)==1:
 print("tiedostonnimi puuttuu!!")
 print("Kaytto: python shp2dxf.py tulostustiedostonimi")
 print("Tiedostonimi ilman paatetta .dxf")
 sys.exit(1)
else:
 tiedostonnimi = sys.argv[1]

#Haetaan kaikki shp-tiedostot tyohakemistosta
tiedostot = []
kohde = []
for files in os.listdir("."):
    if files.endswith(".shp"):
        tiedostot.append(files)
#luetaan shapefile sisaan ja tarkastetaan LUOKKA kentan sijainti shp-tiedoston
#fields-listassa
for lukutiedosto in tiedostot:
 sf =shapefile.Reader(lukutiedosto)
 shapes =sf.fields
 for i in range(0,len(sf.fields)):
  if sf.fields[i][0]=="LUOKKA":
   a = i-1

 #Sitten luetaan tiedot srecs-muuttujaan
 srecs =sf.shapeRecords()
 z =[]

 #Tarkastetaan kohteen geometriatyyppi ja lisataan kohteen tiedeto kohde-listaan
 for i in range(0,len(srecs)):
  if srecs[0].shape.shapeType==1:
   tyyppi = "POINT"
  elif srecs[0].shape.shapeType==3:
   tyyppi = "POLYLINE"
  elif srecs[0].shape.shapeType== 5:
   tyyppi = "POLYGON"
  else:
    continue
  if tyyppi =="POINT":
   kohde.append([srecs[i].record[a], tuple(srecs[i].shape.points[0]), tyyppi])
  else:
   #tassa kaydaan lapi moniosaisen kohteen osat yksi kerrallaan (viivat ja polygonit)
   for g in range(0,len(srecs[i].shape.parts)):
    osa = srecs[i].shape.parts
    if g == (len(srecs[i].shape.parts)-1):
     for j in srecs[i].shape.points[osa[g]:]:
      z.append(tuple(j))
    else:
     for j in srecs[i].shape.points[osa[g]:osa[g+1]]:
      z.append(tuple(j))
    osa = []
    kohde.append([srecs[i].record[a], z, tyyppi])
    z = []
#Ok eli kohde sisaltaa nyt sitten kaiken tarvittavan tallentamista varten
#Seuraavaksi dxf-kirjoituksen pariin
from dxfwrite import DXFEngine as dxf
drawing =dxf.drawing(str(tiedostonnimi)+".dxf")

for i in kohde:
 if i[2]=="POLYLINE":
  polyline=dxf.polyline(linetype='DOT', layer=str(i[0]))
  polyline.add_vertices(i[1])
  polyline.close(False)
  drawing.add(polyline)
 if i[2]=="POLYGON":
  polyline=dxf.polyline(linetype='DOT', layer=str(i[0]))
  polyline.add_vertices(i[1])
  polyline.close(True)
  drawing.add(polyline)
 if i[2] =='POINT':
  point=dxf.point(i[1], layer=str(i[0]))
  drawing.add(point)
drawing.save()

